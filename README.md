# README #

I am a bit of a tab fiend and I end up bookmarking loads of tabs. I want a way to search through the bookmarks and remove duplicates based one their URL.

## Running the code ##
This is going to be a standalone PHP file that you can run on your localhost. It is originally designed to work with just Chrome bookmarks but I might add the ability to process Firefox or IE bookmarks in a similar way.

## Wishlist ##
* Process BM files from different browsers (FF, Safari, IE, etc)
* Optional ping to check the URL is still valid.