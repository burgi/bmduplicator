<?php
namespace Tumblr;

class General{
    public $apikey;
    public $blogname;
    public $secret;
    public $tags=array();
    protected $dbconn;

    public function __construct() {
        //$this->data=$this->getSettingsData();
        $this->setSettingsData();
        $this->dbconn=$this->databaseConnection();
    }
    
    private function getSettingsData(){
        $path='settings';
        
        $settingsFile=fopen($path,'r');
        $contents=fread($settingsFile,  filesize($path));
        $privatedata=explode(";", $contents);
        
        foreach($privatedata as $parameter){
            if(!empty($parameter)){
                $setting=explode("=",trim($parameter));
                $array[$setting[0]]=$setting[1];
            }
        }
                
        return $array;
    }
    
    private function openSettingsFile(){
        $path='settings';
        
        $settingsFile=fopen($path,'r');
        $contents=fread($settingsFile,  filesize($path));
        $privatedata=explode(";", $contents);
        
        foreach($privatedata as $parameter){
            if(!empty($parameter)){
                $setting=explode("=",trim($parameter));
                $array[$setting[0]]=$setting[1];
            }
        }
                
        return $array;
    } 
    
    private function setSettingsData(){
        $data=$this->openSettingsFile();
     
        if(is_array($data)){
            foreach ($data as $key=>$val) {
                if($key=="consumerSecret"){
                    $this->secret=$val;
                }
                if($key=="blogName"){
                    $this->blogname=$val;
                }
                if($key=="apiKey"){
                    $this->apikey=$val;
                }
            }
        }
    }
    
    protected function getPosts(int $offset=0){
        $blogname=$this->blogname;
        $key=$this->apikey;
                
        $url="https://api.tumblr.com/v2/blog/".$blogname.".tumblr.com/posts?api_key=".$key."&offset=".$offset;

        $json=file_get_contents($url);
        $data= json_decode($json);
        
        if($data->meta->msg=='OK'){
            return $data->response->posts;
        }
        
        return false;
    }
    
    protected function collectTags() {
        $posts=$this->getPosts();
            
        foreach($posts as $post){
            $tags=$post->tags;
            
            foreach($tags as $tag){
                $this->tags[]= strtolower($tag);
            }
        }
        return $this->tags;
    }
    
    public function countTags(){
        $tags=$this->collectTags();
        
        $cleanlist=array();
        
        foreach($tags as $tag){
            if(isset($cleanlist[$tag])){
                $cleanlist[$tag]+=1;
            }else{
                $cleanlist[$tag]=1;
            }
        }
        ksort($cleanlist);
        
        return $cleanlist;
    }
    
    protected function databaseConnection() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname="tumblr";

        // Create connection
        $conn = new \mysqli($servername, $username, $password,$dbname);

        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        
        return $conn;
    }
    
    protected function insertTagsToDatabase($tags){
        //$tags=$this->tags;
        
        $sql="";
        var_dump($tags);
        foreach($tags as $key=>$value){
            $sql.="INSERT INTO tags (tag,count) VALUES('".$key."','".$value."');";
        }
        
        $this->dbconn->multi_query($sql);
    }
    
    public function process() {
        $tags=$this->countTags();
        $this->insertTagsToDatabase($tags);
        return "DONE";
    }
    
}